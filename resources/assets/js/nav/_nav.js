export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
      badge: {
        variant: 'primary',
        text: 'NEW'
      }
    },
    {
      title: true,
      name: 'Moduls',
      class: '',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'Users & Groups',
      url: '/components',
      icon: 'icon-puzzle',
      children: [
        {
          name: 'Manage Users',
          url: '/components/buttons',
          icon: 'icon-puzzle'
        },
        {
          name: 'Create User',
          url: '/components/social-buttons',
          icon: 'icon-puzzle'
        },
        {
          name: 'Manage Groups',
          url: '/components/social-buttons',
          icon: 'icon-puzzle'
        },
        {
          name: 'Create Group',
          url: '/components/social-buttons',
          icon: 'icon-puzzle'
        }
      ]
    },
    {
      name: 'Entity',
      url: '/entity',
      icon: 'icon-star',
      children: [
        {
          name: 'Manage Tenant',
          url: '/entity',
          icon: 'icon-star',
          badge: {
            variant: 'secondary',
            text: '4.7'
          }
        },
        {
          name: 'Create new tenant',
          url: '/entity/create',
          icon: 'icon-star'
        },
        {
          name:"Manage Levels",
          url:"/entity/level/",
          icon: 'icon-star'
        },
        {
          name:"Manage Groups",
          url:"/entity/group/",
          icon: 'icon-star'
        }
      ]
    },
    {
      name: 'Access',
      url: '/icons',
      icon: 'icon-lock'
    }
  ]
}
    // },
    // {
    //   name: 'Widgets',
    //   url: '/widgets',
    //   icon: 'icon-calculator',
    //   badge: {
    //     variant: 'primary',
    //     text: 'NEW'
    //   }
    // },
    // {
    //   name: 'Charts',
    //   url: '/charts',
    //   icon: 'icon-pie-chart'
    // },
    // {
    //   divider: true
    // },
    // {
    //   title: true,
    //   name: 'Extras'
    // },
    // {
    //   name: 'Pages',
    //   url: '/pages',
    //   icon: 'icon-star',
    //   children: [
    //     {
    //       name: 'Login',
    //       url: '/pages/login',
    //       icon: 'icon-star'
    //     },
    //     {
    //       name: 'Register',
    //       url: '/pages/register',
    //       icon: 'icon-star'
    //     },
    //     {
    //       name: 'Error 404',
    //       url: '/pages/404',
    //       icon: 'icon-star'
    //     },
    //     {
    //       name: 'Error 500',
    //       url: '/pages/500',
    //       icon: 'icon-star'
    //     }
    //   ]
    // }
  

