
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
/*
require('./bootstrap');

window.Vue = require('vue');
*/
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
//import App from '../../coreui/src/App.vue' 
import router from '../js/router' 
import axios from 'axios'
import VueAxios from 'vue-axios'
import {axios_http} from './components/tenant/axios_http'
import VeeValidate from 'vee-validate'
import network from "./network"

 
import 'element-ui/lib/theme-chalk/index.css'
import Element from 'element-ui'
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'

locale.use(lang)

Vue.use(Element);

var _ = require('lodash'); 


Vue.use(VeeValidate); 


  
 


axios.defaults.headers.common = {
  'X-Requested-With': 'XMLHttpRequest',
};

Vue.use(VueAxios, axios)  

Vue.use(BootstrapVue) 


//Vue.use(axios_http);
/** Tenant Application components */
require("./components/tenant/tenantApp");


new Vue({
  el: '#app',
  data:{
    network:new network()
  },
  //render: h => h(App),
  router,
  //template: '<App/>',
  components: {
  },
  methods:{
    
  }
})


// Include CoreUI JS
//require('../../coreui/src/main.js');

