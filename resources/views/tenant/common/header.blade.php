<!DOCTYPE html>
<html  lang="{{ config('app.locale') }}">
    <!-- dir="rtl" !-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Laravel - CoreUI Example</title>

    <!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="{{url('smart/css')}}/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="{{url('smart/css')}}/font-awesome.min.css">

		<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
		<link rel="stylesheet" type="text/css" media="screen" href="{{url('smart/css')}}/smartadmin-production-plugins.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="{{url('smart/css')}}/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="{{url('smart/css')}}/smartadmin-skins.min.css">

		<!-- SmartAdmin RTL Support is under construction-->
		<link rel="stylesheet" type="text/css" media="screen" href="{{url('smart/css')}}/smartadmin-rtl.min.css"> 
		<link rel="stylesheet" type="text/css" media="screen" href="{{url('smart/css')}}/your_style.css">

		<!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="{{url('smart/css')}}/your_style.css"> -->

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="{{url('smart/css')}}/demo.min.css">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href=" {{url('smart/img')}}/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href=" {{url('smart/img')}}/favicon/favicon.ico" type="image/x-icon">

		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

		<!-- Specifying a Webpage Icon for Web Clip 
			 Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
		<link rel="apple-touch-icon" href=" {{url('smart/img')}}/splash/sptouch-icon-iphone.png">
		<link rel="apple-touch-icon" sizes="76x76" href=" {{url('smart/img')}}/splash/touch-icon-ipad.png">
		<link rel="apple-touch-icon" sizes="120x120" href=" {{url('smart/img')}}/splash/touch-icon-iphone-retina.png">
		<link rel="apple-touch-icon" sizes="152x152" href=" {{url('smart/img')}}/splash/touch-icon-ipad-retina.png">
		
		<!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		
		<!-- Startup image for web apps -->
		<link rel="apple-touch-startup-image" href=" {{url('smart/img')}}/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
		<link rel="apple-touch-startup-image" href=" {{url('smart/img')}}/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
		<link rel="apple-touch-startup-image" href=" {{url('smart/img')}}/splash/iphone.png" media="screen and (max-device-width: 320px)">




    <script>
        window.Laravel = {!! json_encode([
			'csrfToken' => csrf_token(),
		]) !!};
    </script>

    <!-- BODY options, add following classes to body to change options

    // Header options
    1. '.header-fixed'					- Fixed Header

    // Sidebar options
    1. '.sidebar-fixed'					- Fixed Sidebar
    2. '.sidebar-hidden'				- Hidden Sidebar
    3. '.sidebar-off-canvas'		- Off Canvas Sidebar
    4. '.sidebar-compact'				- Compact Sidebar Navigation (Only icons)

    // Aside options
    1. '.aside-menu-fixed'			- Fixed Aside Menu
    2. '.aside-menu-hidden'			- Hidden Aside Menu
    3. '.aside-menu-off-canvas'	- Off Canvas Aside Menu

    // Footer options
    1. '.footer-fixed'						- Fixed footer

    -->
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">


<div id="app">