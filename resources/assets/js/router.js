import Vue from 'vue'

import Container from './components/tenant/container/Full'

import Lang from './components/tenant/container/lang'

import Dashboard from './components/tenant/dashboard/Dashboard'

import ShowEntity from './components/tenant/entity/Show'

import CreateEntity from './components/tenant/entity/Create'

import ManageSubTenantGroup from './components/tenant/entity/groups/Manage'

// import ManageEntityLevel from './components/tenant/entity/levels/Manage'

import ManageEntityType from './components/tenant/entity/types/Manage'

import Logout from './components/tenant/Logout'

import Users from './components/tenant/users/show'

import Router from 'vue-router'
Vue.use(Router) 
const routes = [
    { 
        path: '/',
        name: 'Home',
        redirect:'/dashboard',
        component: Dashboard,
        children: [
          {
            path: 'dashboard',
            name: 'dashboard',
            component: Dashboard
          }
          
          
         
          // {
          //   path: 'charts',
          //   name: 'Charts',
          //   component: Charts
          // },
          // {
          //   path: 'widgets',
          //   name: 'Widgets',
          //   component: Widgets
          // },
          // {
          //   path: 'components',
          //   redirect: '/components/buttons',
          //   name: 'Components',
          //   component: {
          //     render (c) { return c('router-view') }
          //   },
          //   children: [
          //     {
          //       path: 'buttons',
          //       name: 'Buttons',
          //       component: Buttons
          //     },
          //     {
          //       path: 'social-buttons',
          //       name: 'Social Buttons',
          //       component: SocialButtons
          //     },
          //     {
          //       path: 'cards',
          //       name: 'Cards',
          //       component: Cards
          //     },
          //     {
          //       path: 'forms',
          //       name: 'Forms',
          //       component: Forms
          //     },
          //     {
          //       path: 'modals',
          //       name: 'Modals',
          //       component: Modals
          //     },
          //     {
          //       path: 'switches',
          //       name: 'Switches',
          //       component: Switches
          //     },
          //     {
          //       path: 'tables',
          //       name: 'Tables',
          //       component: Tables
          //     }
          //   ]
          // },
          // {
          //   path: 'icons',
          //   redirect: '/icons/font-awesome',
          //   name: 'Icons',
          //   component: {
          //     render (c) { return c('router-view') }
          //   },
          //   children: [
          //     {
          //       path: 'font-awesome',
          //       name: 'Font Awesome',
          //       component: FontAwesome
          //     },
          //     {
          //       path: 'simple-line-icons',
          //       name: 'Simple Line Icons',
          //       component: SimpleLineIcons
          //     }
          //   ]
          // }
        ],  
        
      },
      {
        path: '/entity',
        name: 'entity',
        component: ShowEntity 
      },
      {
        path:'/entity/create',
        name: 'Create Entity',
        component:CreateEntity
      },
      // {
      //   path:'/entity/level',
      //   name: 'Manage Level',
      //   component:ManageEntityLevel
      // },
      {
        path:'/entity/group',
        name:'Manage Group',
        component:ManageSubTenantGroup
      },
      {
        path:'/entity/type',
        name:'Manage Entity Type',
        component:ManageEntityType
      },
      {
        path:'/users',
        name:'Users Mangement',
        component:Users
      },
      {
        path:'/lang',
        name:'lang Mangement',
        component:Lang
      }
      ,
      {
        path: '/logout',
        name: 'logout',
        component: Logout
      }
  ]
  // export default new Router({
  //   mode: 'hash',
  //   routes
  // })
  export default new Router({
    base:"/",
    mode: 'hash', 
    routes: routes
  })


 
