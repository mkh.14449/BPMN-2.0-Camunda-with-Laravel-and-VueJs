import {store} from './components/tenant/store'
import axios from 'axios';
class network {
    /**
     * Create instances.
     */
    constructor() {
        
        this.base_url_addjust = store.state.baseUrl;
        this.base_url_api = this.addjustlinks() + '/api/' + "client/" +store.state.tenant + '/admin/';
        this.base_url_admin = this.addjustlinks() + '/admin/';
        this.base_storage = this.addjustlinks() + '/storage/cases/';

        this.services = {
            "logout" : {"url" : "logout" , 'method' : 'get'},
            "tenantProducts": { 'url': 'tenantProducts', 'method': 'get' },
            
            "subtenant":{'url' : "subtenant" , 'method' : 'get'},
            "createSubtenant":{'url' : "subtenant" , 'method' : 'post'},
            "updateSubtenant":{'url' : "subtenant" , 'method' : 'put'},
            "deleteSubtenant":{'url' : "subtenant" , 'method' : 'delete'},

            "tenantLevel":{'url' : "tenantLevel" , 'method' : 'get'},
            "createLevel":{'url' : "tenantLevel" , 'method' : 'post'},
            "updateLevel":{'url' : "tenantLevel" , 'method' : 'put'},
            "deleteLevel":{'url' : "tenantLevel" , 'method' : 'delete'},
            
            "tenantGroup":{'url' : "tenantGroup" , 'method' : 'get'},
            "createGroup":{'url' : "tenantGroup" , 'method' : 'post'},
            "updateGroup":{'url' : "tenantGroup" , 'method' : 'put'},
            "deleteGroup":{'url' : "tenantGroup" , 'method' : 'delete'},

            "tenantType":{'url' : "tenantType" , 'method' : 'get'},
            "createType":{'url' : "tenantType" , 'method' : 'post'},
            "updateType":{'url' : "tenantType" , 'method' : 'put'},
            "deleteType":{'url' : "tenantType" , 'method' : 'delete'},

            "getusers":{'url' : "users" , 'method' : 'get'},
            "createuser":{'url' : "users" , 'method' : 'post'},
            "updateuser":{'url' : "users" , 'method' : 'put'},
            "deleteuser":{'url' : "users" , 'method' : 'delete'},

            "setlang":{'url' : "setlang" , 'method' : 'post'}
            

            

        };
        this.token = store.state.apiToken ;
        this.headers = {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + store.state.apiToken
        };
    }

    addjustlinks() {
        return this.base_url_addjust;
    }

    getservice(servicename) {
        return this.services[servicename];
    }

    make(servicename, parameters = '', data = '') {
        var servicename_data = this.getservice(servicename);
        return axios.request({
            // `url` is the server URL that will be used for the request
            url: servicename_data.url + parameters,

            // `method` is the request method to be used when making the request
            method: servicename_data.method, // default

            // `baseURL` will be prepended to `url` unless `url` is absolute.
            // It can be convenient to set `baseURL` for an instance of axios to pass relative URLs
            // to methods of that instance.
            baseURL: this.base_url_api,
            // `headers` are custom headers to be sent
            headers: this.headers,

            // to send data post
            data: data

        }).catch(function(error) {
            alert(error);
            window.location.href = store.state.appUrl + "/" + store.state.tenant ;
            // this.catcherror(error);
            // if (error.response.status === 401) {
            //     // The request was made and the server responded with a status code
            //     // that falls out of the range of 2xx
            //     console.log(error.response.data);
            //     console.log(error.response.status);
            //     console.log(error.response.headers);

            //     this.catcherror(error);
            // } else if (error.request) {
            //     // The request was made but no response was received
            //     // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            //     // http.ClientRequest in node.js
            //     console.log(error.request);
            // } else {
            //     // Something happened in setting up the request that triggered an Error
            //     console.log('Error', error.message);
            // }
            // console.log(error.config);
            // return;
        });
    }
    catcherror(error) {

    }
    baseurl() {
        return this.base_url_admin;
    }
    base_url_api() {
        return this.base_url_api;
    }
}
export default network;