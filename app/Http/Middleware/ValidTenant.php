<?php

namespace App\Http\Middleware;

use Closure;
use File ;
use Lang ;
use App\Helpers\CamundaRest;
use Illuminate\Http\Request;
use Auth ;
use App\Models\Tenant\TenantProdAccess;
use App\Models\Tenant\Tenant;
use App\Helpers\Api\GenerateUrl;
use \Redis ;
use \Session ;
class ValidTenant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
  
    public $userTypes = [
        "admin",
        "user"
    ];

    public function __construct(Request $request){
        
    }
    public function handle($request, Closure $next)
    {
        //dd(\Redis::get('tenant_google'));
        //Redis::set("tenant_google",'');
        
        // $redis_data = Redis::get(Redis::get('user_'.Session::get('user_data.user.id')));
        // $tenant = '';
        // //dd(Session::get('user_data.user.id'));
        // dd($redis_data) ;
        
        //dd(Redis::get('tenant'));
        if($this->validateTenant($request,$tenant = '') != true){
            //return redirect('/InavlidTenant');
            return abort(404);
        }

        return $next($request);
    }

    public function validateTenant($request,$tenant = ''){
        
        //dd(Auth::guard('admin')->user());
        
        

        $parameters = $request->route()->parameters();
        if(isset($parameters['tenant'])){
            $tenant = CamundaRest::getSingleTanent($parameters['tenant']);
            
            if(isset($tenant->id) AND ($tenant->id === $parameters['tenant'])){
                session(['active_tanent' => $tenant->id] );


                // if (Auth::guard('admin')->check()) {
                //     if(Auth::guard('admin')->user()->Tenants_idTenants != Tenant::where('TenantBPMRef',$tenant->id)->get()[0]->idTenants){
                //         return false ;
                //     }else{
                //         /** Set id to active tenant in MySql DB not Camunda */
                //         session(['active_tanent_app_id' => Tenant::where('TenantBPMRef',$tenant->id)->get()[0]->idTenants] );

                        
                //     }
                // }
                
                //dd($tenant->id);
                /** Check and create language files */
                //$this->validateTenantComponents($tenant->id);

                $this->tenantLangRedis($tenant->id);
               
                
                return true ;
            }
        }

        

        session(['active_tanent' => null]);
        session(['active_tanent_app_id' => null]);
        session(['tenant' => null]);
        return false ;
    }

    public function validateUserType($request){
        $parameters = $request->route()->parameters();
        if(isset($parameters['userType']) AND in_array($parameters['userType'],$this->userTypes)){
            return true ;
        }
        return false ;
    }

    public function validateTenantComponents($tenant){

        /** Define paths of folder and files */
        $path = realpath(__DIR__.'/../../../resources/lang/tenant/') ;
        $fullPath = $path."/".$tenant ;
        $file_full_path_ar = $fullPath."/ar.php";
        $file_full_path_en = $fullPath."/en.php";

        if(!is_dir($fullPath)){
            File::makeDirectory($fullPath, $mode = 0777, true, true);

            $lang = [];
            $lang['ar'] = Lang::get('default-ar',[],'tenant');
            $lang['en'] = Lang::get('default-en',[],'tenant');

            foreach($lang as $key => $value){
                if($key == 'ar'){
                    File::put($file_full_path_ar,'<?php return '.var_export( $lang['ar'], true ).";\n");
                }elseif($key == 'en'){
                    File::put($file_full_path_en,'<?php return '.var_export( $lang['en'], true ).";\n");
                }
            }
        }   
    }

    public function tenantLangRedis($tenant){
        $redisLang = Redis::get("tenant_".$tenant);
        // if(isset($redisLang) && !empty($redisLang)){
        //     return $redisLang ;
        // }else{
           
            $lang = json_encode(Lang::get('default-ar',[],'tenant'));
            $lang = json_decode($lang,true);
            //$lang['en'] = Lang::get('default-en',[],'tenant');
            $redisLang = Redis::set("tenant_".$tenant,json_encode($lang,true));
            
        //}
    }
    
}
