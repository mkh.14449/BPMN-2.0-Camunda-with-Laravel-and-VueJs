<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('custom/user')->user();

    //dd($users);

    return view('customuser.home');
})->name('home');

