import Example from '../components'

import Header from '../components/Header'
import Sidebar from '../components/Sidebar'
import Aside from '../components/Aside'
import Footer from '../components/Footer'
import Breadcrumb from '../components/Breadcrumb'
import Callout from '../components/Callout'
import Switch from '../components/Switch' 
    


export {
  Aside,
  Breadcrumb,
  Callout,
  Footer,
  Header,
  Sidebar,
  Switch
}

// import Aside from './Aside.vue'
// import Breadcrumb from './Breadcrumb.vue'
// import Callout from './Callout.vue'
// import Footer from './Footer.vue'
// import Header from './Header.vue'
// import Sidebar from './Sidebar.vue'
// import Switch from './Switch.vue'

// export {
//   Aside,
//   Breadcrumb,
//   Callout,
//   Footer,
//   Header,
//   Sidebar,
//   Switch
// }
