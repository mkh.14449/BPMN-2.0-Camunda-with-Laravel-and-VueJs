import network from '../../../network'
import TreeView from './Treeview'
import Groups from './groups/Manage'

export default{
    created(){
        this.getSubTenants();
        this.get_Groups()
        this.get_types()
    },
    mounted(){ 
        
    },
    data(){
        return {
          network:new network(),
          subTenants:'',
          singleSub:'',
          singleSubCreated:false,
          groups:{},
          types:{},
          treeLenght:0,
          loading : this.$loading({
            lock: true,
            text: 'Loading',
            spinner: 'el-icon-loading',
            background: 'rgba(0, 0, 0, 0.7)'
          })
        }
      },
    methods:{
        getSubTenants(){
            /** loading */
            this.loading
            this.network.make('subtenant')
            .then(response => {
              if(response.data.code == 200){
                    setTimeout(() => {
                        this.loading.close();
                  }, 1000);
                
                this.subTenants = response.data.subTenants ;
              }else{
                this.subTenants = [] ;
              }
            })
            .catch(e => {
              this.errors.push(e)
            })
        },
        updateSub(event){
           
            this.network.make('updateSubtenant','/'+this.singleSub.idSubTenant,this.singleSub)
            .then(response => {
              if(response.data.code == 200){
                this.$notify({
                  title: 'Success',
                  message: 'Entity has been updated .',
                  type: 'success'
              });

                this.getSubTenants();
              }else{
                //this.subTenants = [] ;
              }
            })
            .catch(e => {
              this.errors.push(e)
            })
        },
        get_Groups(){
            this.network.make('tenantGroup')
              .then(response => {
                if(response.data.code == 200){
                  this.groups = response.data.subTenantGroups ;
                }else{
                  this.groups = [] ;
                }
              })
              .catch(e => {
                this.errors.push(e)
              })
        },
        get_types(){
            this.network.make('tenantType')
              .then(response => {
                if(response.data.code == 200){
                  this.types = response.data.SubTenantType ;
                }else{
                  this.types = [] ;
                }
              })
              .catch(e => {
                this.errors.push(e)
              })
        }

    },
    watch:{
        subTenants:{
            handler:function(){
               // console.log('parent subtenants changed');
            },
            deep:true
        },
        singleSub:{
            handler:function(){
                this.loading
                this.singleSub = this.singleSub ;
                //console.log(this.singleSub)
            },
            deep:true
        }
        
    }

}