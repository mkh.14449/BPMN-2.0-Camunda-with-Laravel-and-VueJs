<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use App\Helpers\Api\GenerateUrl;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'home';
    protected $loginPath  = "login";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        
        $this->middleware('guest_api')->except('logout');
    }


    public function login(Request $request){
        
        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);

        $http = new \GuzzleHttp\Client;

        $response = $http->post(GenerateUrl::ApiLink('login'), [
            'form_params' => [
                'email' => $request->email,
                'password' => $request->password
            ],
        ]);

        $result = json_decode($response->getBody(),true);
        if($result['code'] == 200){

            $request->session()->put('user_data',json_decode(Redis::get('user_'.$result['data']['user']['id']),true));
            //return Redis::get('user_'.$result['data']['user']['id']);
            return redirect(GenerateUrl::AdminLink($this->redirectTo));
        }else{
            $request->session()->put('user_data' , null) ;
            return redirect(GenerateUrl::AdminLink($this->loginPath))
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'email' => 'Incorrect email address or password',
            ]);
        }
    }

    public function logout(Request $request){
        $request->session()->put('user_data','');
        //dd(Redis::get('user_'.session('user_data.id')));
        Redis::set('user_'.session('user_data.id') , '');
        return redirect(GenerateUrl::AdminLink($this->loginPath));
    }
}
