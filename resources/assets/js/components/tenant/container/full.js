import {store} from '../store'
import Logout from "../Logout" 

var $ = require('jquery');
export default {
  name: 'container',  
  store,store,  
  created(){
    /** Set Store Global Variables */
    this.$store.state.apiToken = this.user.user.token ;
    this.$store.state.tenant = this.tenant ;
    this.$store.state.user = this.user.user ;
    this.$store.state.languages = this.languages ;

    if(!localStorage.getItem('lang')){
        localStorage.setItem('lang' , this.$store.state.defaultLocale);
        this.$store.state.activeLang = this.$store.state.defaultLocale ;
    }else{
      if(localStorage.getItem('lang') == 'ar'){
        $('body').addClass('smart-rtl');
      }
      this.$store.state.activeLang = localStorage.getItem('lang') ;
      this.activeLang = localStorage.getItem('lang') ; 
      
    }
    

    
  },
  props:{
    products:{},  
    tenant:{},
    user:{},
    languages:{},
    
  },
  data () { 
    return {
      // nav: nav.items,
      user_data:this.user.user,
      tokenFirstTime:0,
      activeLang : this.$store.state.activeLang
      
    }
  },
  computed: {
    name () {
      return this.$route.name
    },
    list () {
      return this.$route.matched
    }
  },
  watch:{
    languages:{
      handler(){
        this.$store.state.languages = this.languages 
        console.log(this.languages)
      },
      deep:true
    },
    activeLang:{
      handler(){
        this.$store.state.activeLang = this.activeLang 
        localStorage.setItem('lang' , this.activeLang );
      },
      deep:true
    }
    
  }
}