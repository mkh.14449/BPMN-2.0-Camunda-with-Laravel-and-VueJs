import axios from 'axios';

// Get saved data from sessionStorage
var Token = sessionStorage.getItem('te');
// Remove saved data from sessionStorage
sessionStorage.clear();

export const axios_http = axios.create({
  baseURL: "http://localhost:9000/api/",
  headers: {
    Accept:"application/json",
    Authorization: 'Bearer '+ Token
  }
})