<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Api\GenerateUrl;


class Guest_api
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$redisData = json_decode(\Redis::get('user_'.$request->session()->get('user_data.user.id')),true);

	    if ($request->session()->get('user_data.user.token') AND ($request->session()->get('user_data.user.token') === $redisData['user']['token'])) {
			return redirect(GenerateUrl::AdminLink("home"));
		}
	    return $next($request);
	}
}