import network from '../../../../network'

export default { 
    mounted(){
        this.get_types();
    },
    data(){
      return {
        network:new network(),
        types:'',
        checked:0,
        alert:false,
        c_type:{
          SubTenantType:'',
          Description:'',
        },
        creationMode:false,
        validateMsg:"Please make sure all fields are filled out correctly"
      }
    },
    methods: {
      /** Get Tenant Products */
    get_types(){
      this.network.make('tenantType')
        .then(response => {
          if(response.data.code == 200){
            this.types = response.data.SubTenantType ;
          }else{
            this.types = [] ;
          }
        })
        .catch(e => {
          this.errors.push(e)
        })
    },
    createType(){
      if(this.errors.any()){
        alert(this.validateMsg);
        return ;
      }
     
      this.network.make('createType','',this.c_type,this.c_type)
        .then(response => {
          if(response.data.code == 200){
            this.types.push(this.c_type);
            this.c_type = {
              SubTenantType:'',
              Description:'',
            };
            this.showAlert('success');
            this.changeMode();

          }else{
            //this.levels = [] ;
          }
        })
        .catch(e => {
          this.errors.push(e)
        })
    },
    updateType(type){

      if(this.errors.any()){
        alert(this.validateMsg);
        return ;
      }

      this.network.make('updateType',"/"+type.idSubTenantType,type,type)
        .then(response => {
          if(response.data.code == 200){
            this.showAlert('success');
          }else{
            alert('Bad request');
          }
        })
        .catch(e => {
          this.errors.push(e)
        })
    },
    deleteGroup(type){
      alert('delete');
    },
    showAlert(type){
      if(type == 'success'){
        this.alert = true ;
      }else{
        this.alert = false ;
      }
    },
    changeMode(){
      if(!this.creationMode){
        this.creationMode = true ;
      }else{
        this.creationMode = false ;
      }
    }
    },
    watch:{
      alert:function(){
        if(this.alert){
          let app = this ;
          setTimeout(function(){ app.alert = false }, 3000);
        }
      },
      creationMode:function(){
      }
    }
  }