<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller ;   
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use org\camunda\php\sdk\camundaRestClient ;
use Auth ;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$engineUrl = "http://localhost:8080/engine-rest/engine/default/";
        //$rest = new camundaRestClient("http://localhost:8080/engine-rest/engine/default/");
        
        //return $rest->getTasks();
        // $user = ['name'=>"wael",'email'=>"wael.walid91@gmail.com"];
        // Redis::set('test',json_encode($user));
        // return json_decode(Redis::get('test'),true);
        //$request->session()->flush();
        //dd(Auth::user());
      
        return Redis::get('name');
        return "welcome in home controller ";

        return View('home');
    }
    public function getapi()
    {
        $engineUrl = "http://localhost:8080/engine-rest/engine/default/";
        $rest = new camundaRestClient("http://localhost:8080/engine-rest/engine/default/");
        $params = ['assignee' => 'demo'];
        return $rest->getTasks($params);
        // $user = ['name'=>"wael",'email'=>"wael.walid91@gmail.com"];
        // Redis::set('test',json_encode($user));
        // return json_decode(Redis::get('test'),true);
      
    }

    public function task($id){
        $rest = new camundaRestClient("http://localhost:8080/engine-rest/engine/default/");
        //return response()->json(["data" => $rest->getSingleTask($id)]) ;
        return response()->json(["data" => $rest->restRequest("Get","/task/".$id."/form-variables")]) ; 
        //return $request = file_get_contents("http://localhost:8080/engine-rest/engine/default/task/".$id."/rendered-form");

        
    }
}
