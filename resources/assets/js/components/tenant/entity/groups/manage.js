import network from '../../../../network'

export default { 
    mounted(){
        this.get_Groups();
        //this.get_levels();
    },
    data(){
      return {
        network:new network(),
        groups:'',
        levels:'',
        checked:0,
        alert:false,
        c_group:{
          nameGroupSubTenants:'',
          descGroupSubTenants:''
        },
        creationMode:false,
        validation:false 
      }
    },
    methods: {
      /** Get Tenant Products */
    get_Groups(){
      this.network.make('tenantGroup')
        .then(response => {
          if(response.data.code == 200){
            this.groups = response.data.subTenantGroups ;
          }else{
            this.groups = [] ;
          }
        })
        .catch(e => {
          this.errors.push(e)
        })
    },
    createGroup(){
   
      this.$validator.validateAll().then((result) => {
        if(result == true){
          this.network.make('createGroup','',this.c_group,this.c_group)
            .then(response => {
              if(response.data.code == 200){
                //this.groups.push(this.c_group);
                this.get_Groups();
                this.c_group = {
                  nameGroupSubTenants:'',
                  descGroupSubTenants:''
                };
                this.$notify({
                  title: 'Success ',
                  message: 'Item has been created !',
                  type: 'success',
                  offset: 100,
                  duration:1500
                });
                this.changeMode();

              }else{
                //this.levels = [] ;
              }
            })
            .catch(e => {
              this.errors.push(e)
            })
      }
    });


    },
    updateGroup(group){
      
      this.$validator.validateAll().then((result) => {
        console.log(this.errors.items);
        //return  ;
        if(result && this.errors.items.length < 1){
          this.network.make('updateGroup',"/"+group.idGroupSubTenants,group,group)
          .then(response => {
            if(response.data.code == 200){
              this.$notify({
                title: 'Success ',
                message: 'This Item has been updated !',
                type: 'success',
                offset: 100,
                duration:1500
              });
            }else{
              alert('Bad request');
            }
          })
          .catch(e => {
            this.errors.push(e)
          })
        }else{
          this.$notify({
            title: '! Error ',
            message: 'please fill all inputs as required to submit your action . Thank you',
            type: 'error',
            offset: 100,
            duration:1500
          });
        }
      });
      

      
    },
    updateAll(){
      var app = this ;
      this.groups.forEach(function(group) {
        app.updateGroup(group) ;
      });
    },
    deleteGroup(level){
      alert('delete');
    }
    // get_levels(){
    //   this.network.make('tenantLevel')
    //     .then(response => {
    //       if(response.data.code == 200){
    //         this.levels = response.data.tenantLevels ;
    //       }else{
    //         this.levels = [] ;
    //       }
    //     })
    //     .catch(e => {
    //       this.errors.push(e)
    //     })
    // },
    ,
    showAlert(type){
      if(type == 'success'){
        this.alert = true ;
      }else{
        this.alert = false ;
      }
      
    },
    changeMode(){
      if(!this.creationMode){
        this.creationMode = true ;
      }else{
        this.creationMode = false ;
      }
    }
    },
    computed:{
      'check':function(){
        return {'checked':this.checked,'false':!this.checked};
      }
    },
    watch:{
      alert:function(){
        if(this.alert){
          let app = this ;
          setTimeout(function(){ app.alert = false }, 3000);
        }
      },
      validation:{
        handler(){

        },
        deep:true
      },
      creationMode:function(){
      },
      groups:{
        handler(){

        },
        deep:true
      }
    }
  }