<?php

namespace App\Models\Tenant;

use Illuminate\Database\Eloquent\Model;
use App\Models\Org\OrgProdGroup;
use App\Models\Org\OrgProducts;
use App\Models\Common\OrgProdGroup_OrgProduct;
class TenantProdAccess extends Model
{
    protected $table = "TenantProdAccess";
    protected $primaryKey = "idTenantProdAccess";

    public function getTenantProducts(){
        return $this::
        belongsToMany(OrgProducts::class,"OrgProdGroup_OrgProduct","idOrgProdGroup","idOrgProducts")
        ->join('OrgProdGroup', 'OrgProdGroup.idOrgProdGroup', '=', 'OrgProdGroup_OrgProduct.idOrgProdGroup')
        ->select("OrgProdGroup.*","OrgProducts.*");
    }
}
