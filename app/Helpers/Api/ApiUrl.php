<?php

namespace App\Helpers\Tenant;

use Illuminate\Http\Request;


class GenerateUrl
{
    public function __construct(){
        $this->middleware('auth');
    }
    public static function createLink($var){
        
        //dd(session('active_tanent'));
        if(session('active_tanent')){
            return url("client/".session('active_tanent')."/admin/".$var);
        }
        return null ;
    }

    public static function AdminLink($var){
        if(session('active_tanent')){
            return url("client/".session('active_tanent')."/admin/".$var);
        }
        return null ;
    }

    public static function($var){
        if(session('active_tanent')){
            return url("client/".session('active_tanent')."/admin/".$var);
        }
    }
}
