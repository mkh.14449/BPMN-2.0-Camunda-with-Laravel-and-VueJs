</div>

<script src="{{ mix('js/app.js') }}"></script>

<!-- END SHORTCUT AREA -->

<!--================================================== -->

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }' src="{{url('smart/js')}}/plugin/pace/pace.min.js"></script>

<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
    if (!window.jQuery) {
        document.write('<script src="{{url('smart/js')}}/libs/jquery-2.1.1.min.js"><\/script>');
    }
</script>

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script>
    if (!window.jQuery.ui) {
        document.write('<script src="{{url('smart/js')}}/libs/jquery-ui-1.10.3.min.js"><\/script>');
    }
</script>

<!-- IMPORTANT: APP CONFIG -->
<script src="{{url('smart/js')}}/app.config.js"></script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="{{url('smart/js')}}/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 

<!-- BOOTSTRAP JS -->
<script src="{{url('smart/js')}}/bootstrap/bootstrap.min.js"></script>

<!-- CUSTOM NOTIFICATION -->
<script src="{{url('smart/js')}}/notification/SmartNotification.min.js"></script>

<!-- JARVIS WIDGETS -->
<script src="{{url('smart/js')}}/smartwidgets/jarvis.widget.min.js"></script>

<!-- SPARKLINES -->
<script src="{{url('smart/js')}}/plugin/sparkline/jquery.sparkline.min.js"></script>

<!-- browser msie issue fix -->
<script src="{{url('smart/js')}}/plugin/msie-fix/jquery.mb.browser.min.js"></script>

<!-- FastClick: For mobile devices -->
<script src="{{url('smart/js')}}/plugin/fastclick/fastclick.min.js"></script>

<!-- FastClick: For mobile devices -->
<script src="{{url('smart/js')}}/plugin/fastclick/fastclick.min.js"></script>

<!--[if IE 8]>

<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

<![endif]-->

<!-- Demo purpose only -->
<script src="{{url('smart/js')}}/demo.min.js"></script>

<!-- MAIN APP JS FILE -->
<script src="{{url('smart/js')}}/app.min.js"></script>

<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
<!-- Voice command : plugin -->
<script src="{{url('smart/js')}}/speech/voicecommand.min.js"></script>

<!-- SmartChat UI : plugin -->
<script src="{{url('smart/js')}}/smart-chat-ui/smart.chat.ui.min.js"></script>
<script src="{{url('smart/js')}}/smart-chat-ui/smart.chat.manager.min.js"></script>

<!-- PAGE RELATED PLUGIN(S) -->

<script src="{{url('smart/js')}}/plugin/delete-table-row/delete-table-row.min.js"></script>

<script src="{{url('smart/js')}}/plugin/summernote/summernote.min.js"></script>

<script src="{{url('smart/js')}}/plugin/select2/select2.min.js"></script>

<script type="text/javascript">

$(document).ready(function() {
		
    // DO NOT REMOVE : GLOBAL FUNCTIONS!
    pageSetUp();

    

    // PAGE RELATED SCRIPTS

    /*
     * Fixed table height
     */
    
    //tableHeightSize()
    
    //$(window).resize(function() {
    //	tableHeightSize()
    //})
    
    function tableHeightSize() {

        if ($('body').hasClass('menu-on-top')) {
            var menuHeight = 68;
            // nav height

            var tableHeight = ($(window).height() - 224) - menuHeight;
            if (tableHeight < (320 - menuHeight)) {
                $('.table-wrap').css('height', (320 - menuHeight) + 'px');
            } else {
                $('.table-wrap').css('height', tableHeight + 'px');
            }

        } else {
            var tableHeight = $(window).height() - 224;
            if (tableHeight < 320) {
                $('.table-wrap').css('height', 320 + 'px');
            } else {
                $('.table-wrap').css('height', tableHeight + 'px');
            }

        }

    }
    
    /*
     * LOAD INBOX MESSAGES
     */
    //loadInbox();
    function loadInbox() {
        loadURL("ajax/email/email-list.html", $('#inbox-content > .table-wrap'))
    }

    /*
     * Buttons (compose mail and inbox load)
     */
    $(".inbox-load").click(function() {
        loadInbox();
    });

    // compose email
    $("#compose-mail").click(function() {
        loadURL("ajax/email-compose.html", $('#inbox-content > .table-wrap'));
    })

});	

/** Leftside menu class active */
$('body .leftsideMenu li').each(function(){
        
        $(this).click(function(){
          $(".leftsideMenu li").removeClass('active');
          $(this).addClass('active');
        });
        
})


</script>

</body>
</html>