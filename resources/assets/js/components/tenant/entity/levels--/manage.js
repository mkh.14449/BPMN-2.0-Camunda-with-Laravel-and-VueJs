import network from '../../../../network'

export default {
    mounted(){
        this.get_levels();
    },
    data(){
      return {
        network:new network(),
        levels:'',
        checked:0,
        alert:false,
        c_level:{
          WeightDesc:'',
          LevelWeight:'',
          IsBPMAuth:''
        },
        creationMode:false
      }
    },
    methods: {
      /** Get Tenant Products */
    get_levels(){
      this.network.make('tenantLevel')
        .then(response => {
          if(response.data.code == 200){
            this.levels = response.data.tenantLevels ;
          }else{
            this.levels = [] ;
          }
        })
        .catch(e => {
          this.errors.push(e)
        })
    },
    createLevel(){
      if(this.errors.any()){
        return ;
      }
     
      this.network.make('createLevel','',this.c_level,this.c_level)
        .then(response => {
          if(response.data.code == 200){
            this.levels.push(this.c_level);
            this.c_level = {
              WeightDesc:'',
              LevelWeight:'',
              IsBPMAuth:''
            };
            this.$notify({
              title: 'Success',
              message: 'The entity level has been created',
              type: 'success',
              offset: 100,
              duration:1800
            });
            this.changeMode();

          }else{
            //this.levels = [] ;
          }
        })
        .catch(e => {
          this.errors.push(e)
        })
    },
    updateLevel(level){
      if(this.errors.any()){
        this.$notify({
          title: 'Error ! ',
          message: 'Please make sure all fields are filled out correctly !',
          type: 'error',
          offset: 100,
          duration:3500
        });
        return ;
      }
      
      this.network.make('updateLevel',"/"+level.idSubTenantLevels,level,level)
        .then(response => {
          if(response.data.code == 200){
            this.$notify({
              title: 'Success',
              message: 'The entity level has been updated',
              type: 'success',
              offset: 100,
              duration:1800
            });
          }
          if(response.data.code == 201){
            this.$notify({
              title: 'Warning',
              message: response.data.msg,
              type: 'warning',
              offset: 100,
              duration:1800
            }); 
          }
        })
        .catch(e => {
          this.errors.push(e)
        })
    },
    deleteLevel(level){
      this.network.make('deleteLevel',"/"+level.idSubTenantLevels,level,level)
        .then(response => {
          if(response.data.code == 200){
            this.levels.splice(this.levels.indexOf(level),1);
            this.$notify({
              title: 'Success',
              message: 'The entity level has been deleted',
              type: 'success',
              offset: 100,
              duration:1800
            });
          }else{
            //this.levels = [] ;
          }
        })
        .catch(e => {
          this.errors.push(e)
        })
    },
    showAlert(type){
      if(type == 'success'){
        this.alert = true ;
      }else{
        this.alert = false ;
      }
      
    },
    changeMode(){
      if(!this.creationMode){
        this.creationMode = true ;
      }else{
        this.creationMode = false ;
      }
    }
    },
    computed:{
      'check':function(){
        return {'checked':this.checked,'false':!this.checked};
      }
    },
    watch:{
      alert:function(){
        if(this.alert){
          let app = this ;
          setTimeout(function(){ app.alert = false }, 3000);
        }
      },
      creationMode:function(){
      },
      levels:{
        handler:function(){

        },
        deep:true
      }
    }
  }