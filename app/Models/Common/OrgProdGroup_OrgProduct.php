<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class OrgProdGroup_OrgProduct extends Model
{
    protected $table = "OrgProdGroup_OrgProduct";
    protected $primaryKey = "idOrgProdGroup_OrgProduct" ;
}
