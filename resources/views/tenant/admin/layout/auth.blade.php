@extends('tenant.common.header')
<meta name="csrf-token" content="{{ csrf_token() }}" />

<link href="{{url('tenant')}}/css/pages/login2.css" rel="stylesheet" />

<body>
    
    <!-- <li>
        <a href="#"
            onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
            Logout
        </a>
        <!-- <a href="{{ \App\Helpers\Api\GenerateUrl::createLink('logout') }}">
            Logout
        </a> -->
        
       <!-- <form id="logout-form" action="{{ \App\Helpers\Api\GenerateUrl::createLink('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </li> -->
    

    @yield('content')

    
    @extends('tenant.common.footer')
    <!-- Scripts -->
    <script src="{{url('/')}}/js/app.js"></script>
    <!-- global js -->
    <script src="{{url('tenant')}}/js/app.js" type="text/javascript"></script>
    <!-- begining of page level js-->
    <script src="{{url('tenant')}}/js/TweenLite.min.js"></script>
    <script src="{{url('tenant')}}/vendors/iCheck/js/icheck.js" type="text/javascript"></script>
    <script src="{{url('tenant')}}/js/pages/login2.js" type="text/javascript"></script>
    <!-- end of page level js-->
</body>
